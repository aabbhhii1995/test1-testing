//Student Name: Abhishek
//Student Id : C0735320

import static org.junit.Assert.*;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import java.util.List;

public class SeleniumTest {

	
	WebDriver driver ; 
	
	
	final String URL = "http://blazedemo.com/";
	final String DRIVER_PATH = "/Users/abhishekbansal/Downloads/chromedriver";	
	
	@Before
	public void setUp() throws Exception {
		// setting up the chrome driver
				System.setProperty("webdriver.chrome.driver", DRIVER_PATH);
				
				driver = new ChromeDriver();
				
				driver.get(URL);
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(2000);
		driver.close();
		
	}

	@Test
    public void testdestinationForWeek() {
        
        WebElement weekdestination = driver.findElement(By.linkText("destination of the week! The Beach!"));
        weekdestination.click();
    }

	
	
	@Test
	public void dropdown() throws InterruptedException {
		Select se = new Select(driver.findElement(By.name("fromPort")));
		List<WebElement> listOptionDropdown = se.getOptions();
        int numDestination = listOptionDropdown.size();
        assertEquals(7, numDestination);

	}
	
	
	@Test
    public void flight() {
		
		driver.navigate().to("http://blazedemo.com/reserve.php");
		
		WebElement flight = driver.findElement(By.linkText("Virgin America"));
        flight.click();
		
    }

}
