//Student Name: Abhishek
//Student Id : C0735320
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SnakeTest {
	private Snake peter;
	private Snake takis;

	@Before
	public void setUp() throws Exception {
		peter = new Snake("Peter S", 10, "coffee");
		takis = new Snake("Takis Z", 80, "vegetables");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testHealth() throws Exception {
		
		Boolean takisSnake = takis.isHealthy();
		
		assertTrue(takisSnake);
		
		
		
		Boolean petersSnake = peter.isHealthy();
		
		assertFalse(petersSnake);
			
	}
	
	@Test
	public void testFits() throws Exception {
		//case1: when cage is big and snake is small in size
		//Snake fits in the cage
		//cage = 120, snake = 10; (assertTrue)
		
		
		Boolean tSnakeBigLength = takis.fitsInCage(120);
		assertTrue(tSnakeBigLength);
		
		
		
		
		//case2: when snake is big and cage is small in size
		//Snake does not fits in the cage
		//cage = 100, snake = 120; (assertFalse)
		
		
		Boolean tSnakeSmallLength = takis.fitsInCage(80);
		assertFalse(tSnakeSmallLength);
		
		
		
		
		//case3: when snake is equal to the cage size
		//Snake  fits in the cage
		//cage = 100, snake = 100; (assertEquals)
				
		Boolean tSnakeEqualLength = takis.fitsInCage(100);
		equals (tSnakeEqualLength);
			
	}
	

}
